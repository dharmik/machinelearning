function [] = CSCI567_hw5_fall15()
    warning('off','all');
    % Data pre processing
    load hw5_blob.mat
    blob_data = points;
    load hw5_circle.mat
    circle_data = points;
    fprintf('=======4a K-MEANS=======\n');
    % K-means for blob data
    K = [2,3,5];
    for k=K
        class = k_means(blob_data, k);
        fig_name = sprintf('K-Means of Blob data for k=%d',k);
        figure('Name',fig_name)
        scatter(blob_data(:,1), blob_data(:,2),30,class,'filled');
    end
    
    %K-means for circle data
    for k=K
        [class] = k_means(circle_data, k);
        fig_name = sprintf('K-Means of circle data for k=%d',k);
        figure('Name',fig_name)
        scatter(circle_data(:,1), circle_data(:,2),30,class,'filled');
    end
    
    fprintf('=======Executing Kernel K-Means=======\n');
    kernel_k_means(circle_data, 2);
    
    fprintf('=======Executing EM Algorithm=======\n');
    Gaussian_EM();
end