function [classes] = k_means(data, k)
    m=size(data,1);
    %n=size(data,2);
    %classes = zeros(m,1);
    % Randomly select k points from data as a starting point for mu_k
    mu_k = datasample(data,k,'Replace',false);

    prev_class = ones(m,1);
    present_class = zeros(m,1);
    while any(prev_class ~= present_class)
        prev_class = present_class;
        % find the distance between data and mu_k
        distances = pdist2(data, mu_k);
        % Each row in distances contain the euclidian distance between
        % that data point and all points in mu_k. Find class which
        % has min distance in each row
        [~, present_class] = min(distances, [], 2);
        for i=1:k
            mu_k(i,:) = mean(data(present_class == i,:));
        end
    end
    classes = present_class;
end