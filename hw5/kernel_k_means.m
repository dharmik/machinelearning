function [classes] = kernel_k_means(data, K)
    M = size(data,1);
    %classes=ones(M,1);
    % Init classes
    dist_orig = sum(data.^2,2);
    classes=dist_orig>0.4;
    classes = [classes, 1-classes];

    % Compute Gram matrix
    gram_matrix = pdist2(data, data).^ 2;
    gram_matrix = exp(gram_matrix);

    dist = zeros(M,K);
    prev_b = zeros(M,1);
    while(1)
        N = sum(classes);
        for k = 1:K
            % Compute kernelised distance
            a=(2/(N(k)))*sum(repmat(classes(:,k)',M,1).*gram_matrix,2);
            %size(classes(:,k))
            %size(classes(:,k)')
            t=classes(:,k)*classes(:,k)';
            b=sum(sum((t).*gram_matrix))/N(k)^(+2);
            dist(:,k) = diag(gram_matrix) - a + b;
        end
        prev_class = classes;
        [~, b] = min(dist,[],2);
        if all(b==prev_b)
            break
        end
        classes = [b==1 b==2];
        classes = 1.0 * classes;
        if all(prev_class==classes)
            break;
        end
    end
    % Plot
    figure('Name','kernel K-Means for k=2');hold off
    cols = {'r','b'};
    
    for k = 1:2
        X=data(classes(:,k)==1,1);
        Y =data(classes(:,k)==1,2);
        %figure('Name','kernel K-Means for k=2');
        scatter(X,Y,35, cols{k},'filled')
        hold on;
    end

end