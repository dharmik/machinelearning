function [] = Gaussian_EM()
    load hw5_blob.mat
    data = points;
    k=3;
    M = size(data,1);
    f=size(data,2);
    full_runs = 0;
    while full_runs < 5
        full_runs = full_runs +1;
        full_runs
        %Initilise
        mean_k = datasample(data,k,'Replace',false);
        sigma_k = [];
        for i=1:k
            sigma_k{i} = eye(2);
        end
        prior = ones(1, k) * (1 / k);
        counter = 50;
        log_likelihood = zeros(1,counter);
        run =0;
        max_ll = -99999;

        while run<counter
           run = run + 1;
           % E-step
           weight = ones(M,k);
           for i=1:k
               weight(:,i) = mvnpdf(data,mean_k(i,:),sigma_k{i}) .* prior(i);
           end
           weight = weight ./ repmat(sum(weight,2),1,k);
           % M -step
           for i=1:k
               % update mean
               mean_k(i,:) = weight(:,i)'*data / sum(weight(:,i));
               %update sigma
               sigma_k{i} = (repmat(weight(:,i),1,f) .* data)' * data / sum(weight(:,i));
               %update prior
               prior(i) = mean(weight(:,i));
           end
           log_likelihood(run) = log_like(data,mean_k,sigma_k,prior,k);
           
           % Update log likelihood
           if max_ll < log_likelihood(run)
               max_ll = log_likelihood(run);
               max_mu = mean_k;
               max_sigma = sigma_k;
               max_weight = weight;
           end
        end
        % Plot the log likelihood
        %size(log_likelihood')
        plot([1:length(log_likelihood)],log_likelihood);
        hold on;
    end
    
    % Plot best features
    disp('MEAN')
    disp(max_mu);
    disp('Covariance')
    for i=1:k
        disp(max_sigma{i});
    end
    [~, cluster] = max(max_weight,[],2);
    figure(99)
    scatter(data(:,1), data(:,2),30,cluster','filled');
    
    
end

function [val] = log_like(data,mean,sigma,prior,k)
    val=0;
    for i = 1 : size(data,1)
        tmp = 0;
        for j = 1 : k
            tmp = tmp + prior(j) * mvnpdf(data(i,:), mean(j, :),sigma{j});
        end
        val = val + log(tmp);
    end
end