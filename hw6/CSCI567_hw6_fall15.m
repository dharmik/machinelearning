function [] = CSCI567_hw6_fall15()
    warning('off','all');
    % Data loading
    load('data\hw6_pca.mat');
    % a
    eigenvecs = get_sorted_eigenvecs(X.train);
    % Plot the top 8 eigen vectors
    %x = linspace(-5,5);
    % b
    figure(1);
    for i=1:8
%         figure
%         hold on
        subplot(2,4,i)
        imshow(double(reshape(eigenvecs(:,i), 16, 16)),[]);
%        hold off
    end
    % c
    figure(2)
    samples = [5500 6500 7500 8000 8500];
    %samples = [4900 5900 6900 7900 8900];
    count = 1;
    for i = samples
        subplot(6,5,count)
        count = count +1;
        imshow(double(reshape(X.train(i,:), 16, 16)),[]);
    end
    X_compressed = (X.train - repmat(mean(X.train),size(X.train,1),1)) * eigenvecs;
    for k = [1 5 10 20 80]
        for e = samples
            X_recon = X_compressed(e,1:k)*eigenvecs(:,1:k)';
            X_recon = X_recon + mean(X.train);
            subplot(6,5,count);
            count = count +1;
            imshow(double(reshape(X_recon, 16, 16)),[]);
        end
    end
    % d
    disp('Classification with PCA')
    X_train_nml = X.train - repmat(mean(X.train), size(X.train,1),1);
    X_test_nml = X.test - repmat(mean(X.train), size(X.test,1),1);
    k = 20;
    X_train_comp = X_train_nml * eigenvecs;
    X_test_comp = X_test_nml * eigenvecs;
    K=[1,5,10,20,80];
    for k = K
        tic;
        tree = ClassificationTree.fit(X_train_comp(:,1:k),y.train,'SplitCriterion', 'deviance');
        train_label_inferred = predict(tree,X_train_comp(:,1:k));
        test_label_inferred = predict(tree,X_test_comp(:,1:k));
        t = toc;
        train_acc= sum(y.train == train_label_inferred)/length(y.train);
        test_acc= sum(y.test == test_label_inferred)/length(y.test);
        fprintf ('K=%d: time= %g TrainAccuracy=%f TestAccuracy=%f\n',k,t, train_acc,test_acc);
    end
    
    % 3.2
    hmm();
end