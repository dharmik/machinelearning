function [] = hmm()
    train = load('data/hw6_hmm_train.txt');
    max = 0;
    min = 9999;
    u = unique(train(:,1))';
    for i = u
        count= sum(train(:,1) ==i);
        if max < count
            max = count;
        end
        if min > count
            min = count;
        end
    end
    obv = size(unique(train(:,2)),1);
    fprintf('HMM: LongestTrace=%d, shortestTrace=%d differentObv=%d\n',max,min,obv);
end
