function [data, labels] = generate_data(file)
    load (file);
    labels = label';
    [m,n] = size(features);
    data = zeros(m, n+16);
    cols = [2,7,8,14,15,16,26,29];
    new_col=0;
    for i=1:30
        if sum(cols == i) > 0
            data(:,i+2*new_col:i+2*new_col+2) = get_new_features(features(:,i));
            new_col = new_col + 1;
        else
            data(:,i+2*new_col) = features(:,i);
        end
    end
end


function [new_f] = get_new_features(f)
	m = size(f,1);
	new_f = zeros(m,3);
	for i=1:m
		if f(i,1) == -1
			new_f(i,1) = 1;
		elseif f(i,1) == 0
			new_f(i,2) = 1;
		elseif f(i,1) == 1
			new_f(i,3) = 1;
		end
	end
end