function [] = libsvm()
    train_file = 'phishing-train.mat';
    test_file = 'phishing-test.mat';
    [train, train_label] = generate_data(train_file);
    [test, test_label] = generate_data(test_file);
    C_values = [4^-3 4^-2 4^-1 4^0 4^1 4^2 4^3 4^4 4^5 4^6 4^7 ];
    degree = [1,2,3];
    gamma  = [4^-7 4^-6 4^-5 4^-4 4^-3 4^-2 4^-1];
    best_C=C_values(1,1);
    best_d=degree(1,1);
    best_g=gamma(1,1);
    %f_klib = fopen('kernel_libsvm.txt','w');
    %Polynomial kernel
    %fprintf(f_klib,'===Polynomial kernel using LIBSVM===\n');
    fprintf('===Polynomial kernel using LIBSVM===\n');
    max_accu = 0;
    for C = C_values
        for d = degree
            tic;
            accu = svmtrain(double(train_label), double(train), sprintf('-c %g -v 3 -t 1 -d %f -q',C,d));
            %fprintf(f_klib,sprintf('polyKernelLIBSVM: C=%f , Degree=%f Accuracy=%f time=%g\n',C, d, accu, toc));
            fprintf('polyKernelLIBSVM: C=%f , Degree=%f Accuracy=%f time=%g\n',C, d, accu, toc);
            if max_accu < accu
                max_accu = accu;
                best_C = C;
                best_d = d;
            end
        end
    end
    poly_model = svmtrain(double(train_label), double(train), sprintf('-c %g -t 1 -d %f -q',best_C,best_d));
    [~, test_accu, ~] = svmpredict(double(test_label), double(test), poly_model);
    %fprintf(f_klib,sprintf('polyKernelLIBSVM: Test output: C=%f , Degree=%f Accuracy=%f\n',best_C,best_d, test_accu));
    fprintf('polyKernelLIBSVM: Test output: C=%f , Degree=%f Accuracy=%f\n',best_C,best_d, test_accu);
    
    %RBF kernel
    %fprintf(f_klib,'\n===RBF kernel using LIBSVM===\n');
    fprintf('\n===RBF kernel using LIBSVM===\n');
    max_accu = 0;
    for C = C_values
        for g = gamma
            tic;
            accu = svmtrain(double(train_label), double(train), sprintf('-c %g -v 3 -t 2 -g %f -q',C,g));
            %fprintf(f_klib,sprintf('RBFKernelLIBSVM: C=%f, gamma=%f Accuracy=%f time=%g\n',C, g, accu, toc));
            fprintf('RBFKernelLIBSVM: C=%f, gamma=%f Accuracy=%f time=%g\n',C, g, accu, toc);
            if max_accu < accu
                max_accu = accu;
                best_C = C;
                best_g = g;
            end
        end
    end
    poly_model = svmtrain(double(train_label), double(train), sprintf('-c %g -t 2 -g %f -q',best_C,best_g));
    [~, test_accu, ~] = svmpredict(double(test_label), double(test), poly_model);
    %fprintf(f_klib,sprintf('RBFKernelLIBSVM: Test output: C=%f , Degree=%f Accuracy=%f\n',best_C,best_g, test_accu));
    fprintf('RBFKernelLIBSVM: Test output: C=%f , Gamma=%f Accuracy=%f\n',best_C,best_g, test_accu);
    
    %fclose(f_klib);
    %fprintf('Results of RBF Kernel LIBSVM saved in kernel_libsvm.txt file\n');
end