function [] = linear_libsvm()
    train_file = 'phishing-train.mat';
    [train, label] = generate_data(train_file);
    C_values = [4^-6 4^-5 4^-4 4^-3 4^-2 4^-1 4^0 4^1 4^2];
    fprintf('\n===Linear kernel using LIBSVM===\n');
    %f_lib = fopen('linear_libsvm.txt','w');
    for C = C_values   
       tic;
       accu = svmtrain(double(label), double(train), sprintf('-c %g -v 3 -t 0 -q',C));       
       %fprintf(f_lib, sprintf('LIBSVM: C=%f Accuracy=%f Traintime=%g\n', C, accu, toc));
       fprintf('Linear LIBSVM: C=%f Accuracy=%f Traintime=%g\n', C, accu, toc);
    end
    %fclose(f_lib);
    %fprintf('Results of Linear LIBSVM saved in linear_libsvm.txt file\n');
end