function [] = own_linear()
    train_file = 'phishing-train.mat';
    test_file = 'phishing-test.mat';
    [train_data, train_labels] = generate_data(train_file);
    [test_data, test_labels] = generate_data(test_file);
    %f_lib = fopen('own_linear.txt','w');
    %fprintf ('C=%f Avg time= %g Accuracy=%f\n',C,avg_time, avg_acc);
    C_values = [4^-6 4^-5 4^-4 4^-3 4^-2 4^-1 4^0 4^1 4^2];
    [opt_C, ~] = cross_validation(train_data,train_labels,3,C_values);
    %load phishing-test.mat;
    [w,b] = trainsvm(train_data, train_labels,opt_C);
    [test_accu] = testsvm(test_data, test_labels,w,b);
    fprintf('Test accuracy using my own svm for C=%f is %f\n',opt_C, test_accu);
end