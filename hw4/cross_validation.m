function [opt_C,avg_time] = cross_validation(train_data,labels,N,C_list)
    max_acc = 0;
    for C = C_list
        train_rows = size(train_data,1);
        split_size = fix(train_rows/N);

        start_row = 1;
        count = 0;
        acc = 0;
        time = 0;
        
        for k=1:N
            % Get 1/3 of train data as dev data
            end_row = min([start_row + split_size, train_rows]);
            if start_row > end_row
                break
            end
            count = count+1;
            d_data=train_data(start_row:end_row, :);
            y_t_data = labels;
            y_d_data = y_t_data(start_row:end_row, :);
            t_data = train_data;
            t_data(start_row:end_row, :) = [];
            y_t_data(start_row:end_row, :) = [];
            %[start_row  end_row]
            start_row = end_row + 1;
            tic;
            [w,b] = trainsvm(t_data, y_t_data,C);
            time = time + toc;
            acc = acc + testsvm(d_data, y_d_data,w,b);
        end
        avg_acc = acc/count;
        avg_time  = time/count;
        fprintf ('Own SVM: C=%f Avg time= %g Accuracy=%f\n',C,avg_time, avg_acc);
        if max_acc < avg_acc
            max_acc = avg_acc;
            opt_C = C;
        end
    end
end