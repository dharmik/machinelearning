function accuracy = testsvm(data, label, w, b)
% data: N*D 
% label: D*1
% w: weight vector
% b: bias

    pred = (data * w) + b;
    predict_labels = ((pred>0) + -1*(pred<0));
    accuracy   = (sum(predict_labels==label)/length(label));
end