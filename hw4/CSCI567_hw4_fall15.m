function [] = CSCI567_hw4_fall15()
    warning('off','all');
    % Data pre processing
    %train_file= 'phishing-train.mat';
    %[train_features, train_labels] = generate_data(train_file);
    
    % Test data pre-processing
    %test_file = 'phishing-test.mat';
    %[test_features, test_labels] = generate_data(test_file);
    fprintf('=======Executing my own SVM=======\n');
    % My SVM
    own_linear()
    
    fprintf('=======Executing Linear LIBSVM=======\n');
    % Linear LIBSVM
    linear_libsvm();
    
    fprintf('=======Executing Polynomial/RBF Kernel LIBSVM=======\n');
    % Polynomial/RBF LIBSVM
    libsvm();
end