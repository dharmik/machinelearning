function [w,b] = trainsvm(data, labels, C)
    % x = quadprog(H,f,A,b,Aeq,beq,lb,ub,x0,options)
    data = double(data);
    labels = double(labels);
    [N,D] = size(data);
    %D = size(train_data,2);

    H = zeros(N+D+1);
    f  = zeros(N+D+1,1);
    A = zeros(2*N,N+D+1);
    b = zeros(2*N,1);
    Aeq = [];
    Beq=[];
    ub = Inf* ones(N+D+1,1);
    lb = [-Inf*ones(D,1)];
    lb = [lb; zeros(N,1)];
    lb = [lb; -Inf];
    b(1:2:size(b,1)) = -1;

    for i=1:(N+D+1)/2
        H(i,i) = 1;
    end
    
    A(1:2:size(A,1),size(A,2)) = -1.0 * labels;
    A(1:2:size(A,1),1:D) = -1 * (repmat(labels,1,D).*data);
    for i = 1:N
        A(i,i+D) = -1;
        A(2*i-1,i+D) = -1;
    end

    for i=D+1:N+D
        f(i,1) = C;
    end

    options = optimoptions('quadprog','Algorithm','interior-point-convex','Display','off');
    x = quadprog(H,f,A,b,Aeq,Beq,lb,ub,[],options);
    w = x(1:D);
    b = x(size(x,1),1);
end