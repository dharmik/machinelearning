function [] = CSCI567_hw1_fall15()
    % Pre process Nursery data
    nursery_train_file = 'hw1nursery_train.txt';
    nursery_test_file = 'hw1nursery_test.txt';
    nursery_valid_file = 'hw1nursery_valid.txt';
    [nursery_train_data, nursery_train_label] = pre_process(nursery_train_file);
    [nursery_valid_data, nursery_valid_label] = pre_process(nursery_valid_file);
    [nursery_test_data, nursery_test_label] = pre_process(nursery_test_file);
%     disp(size(nursery_train_data));
%     disp(size(nursery_train_label));
%     disp(size(nursery_valid_data));
%     disp(size(nursery_valid_label));
%     disp(size(nursery_test_data));
%     disp(size(nursery_test_label));
    
    % Pre-Process TTT data
    ttt_train_file = 'hw1ttt_train.txt';
    ttt_test_file = 'hw1ttt_test.txt';
    ttt_valid_file = 'hw1ttt_valid.txt';
    [ttt_train_data, ttt_train_label] = pre_process(ttt_train_file);
    [ttt_valid_data, ttt_valid_label] = pre_process(ttt_valid_file);
    [ttt_test_data, ttt_test_label] = pre_process(ttt_test_file);
%     disp(size(ttt_train_data));
%     disp(size(ttt_train_label));
%     disp(size(ttt_valid_data));
%     disp(size(ttt_valid_label));
%     disp(size(ttt_test_data));
%     disp(size(ttt_test_label));
    
    % Run Naive-Bayes on nursery data
    disp('Executing NAIVE BAYES...');
    [nursery_test_accu, nursery_train_accu] = naive_bayes(nursery_train_data,nursery_train_label,nursery_test_data, nursery_test_label);
    [nursery_valid_accu, nursery_train_accu] = naive_bayes(nursery_train_data,nursery_train_label,nursery_valid_data, nursery_valid_label);
    disp('Nursery test, valid & train accuracy resp. using Naive Bayes:');
    disp(nursery_test_accu);
    disp(nursery_valid_accu);
    disp(nursery_train_accu);
    
    %Run Naive-Bayes on TTT data
    [ttt_test_accu, ttt_train_accu] = naive_bayes(ttt_train_data,ttt_train_label,ttt_test_data, ttt_test_label);
    [ttt_valid_accu, ttt_train_accu] = naive_bayes(ttt_train_data,ttt_train_label,ttt_valid_data, ttt_valid_label);
    disp('Tic-Tac-Toe test, valid & train accuracy using Naive Bayes:');
    disp(ttt_test_accu);
    disp(ttt_valid_accu);
    disp(ttt_train_accu);
    
    % Run K-NN on TTT data
    disp('Executing K-NN...');
    for K=1:2:15
       [ttt_test_accu, ttt_train_accu] = knn_classify(ttt_train_data,ttt_train_label,ttt_test_data, ttt_test_label,K);
       [ttt_valid_accu, ttt_train_accu] = knn_classify(ttt_train_data,ttt_train_label,ttt_valid_data, ttt_valid_label,K);
       disp(K);
       disp(ttt_test_accu);
       disp(ttt_valid_accu);
       disp(ttt_train_accu);
    end
    
    %Run Decision tree on TTT
    disp('Executing Decision Tree...');
    disp('Train, Validation & Test accuracy using Decision tree');
    for sc1 = {'gdi','deviance'}
        sc = sc1{1,1};
        for leaf=1:10
            fprintf('SplitCriterion=%s, MinLeaf=%d\n',sc,leaf);
            % predict for each data set() and get the accuracy
            %train data
            tree = ClassificationTree.fit(ttt_train_data,ttt_train_label,'Prune','off','SplitCriterion',sc,'MinLeaf',leaf);
            train_predict = predict(tree, ttt_train_data);
            train_accu = size(find(train_predict == ttt_train_label),1)./size(ttt_train_label,1);
            disp(train_accu);
            % Validation data
            valid_predict = predict(tree, ttt_valid_data);
            valid_accu = size(find(valid_predict == ttt_valid_label),1)./size(ttt_valid_label,1);
            disp(valid_accu);
            % Test data
            test_predict = predict(tree, ttt_test_data);
            test_accu = size(find(test_predict == ttt_test_label),1)./size(ttt_test_label,1);
            disp(test_accu);
        end
    end

    % Decision Boundary plot
    load hw1boundary;
    test_input = zeros(10000,2);
    i=1;
    for x=0:0.01:1
        for y=0:0.01:1
            test_input(i,:) = [x,y];
            i = i+1;
        end
    end
    for i=[1,5,15,25]
        figure(i);
        op_labels = knn_2(features, labels, test_input,i);
        for j=1:size(test_input,1)
            if op_labels(j,1) == 1
                scatter(test_input(j,1),test_input(j,2),40,'b');
                hold on;
            else
                scatter(test_input(j,1),test_input(j,2),40,'y');
                hold on;
            end
        end
    end
    
    de()
    de_variance()
end

function [knn_output] = knn_2(train_data, train_label, new_data,K)
    % Normalise the training data
    [m,n] = size(train_data);
    train_mean = mean2(train_data);
    train_std = std2(train_data);
    %disp(train_mean);
    train_data_nml = zeros(m,n);
    for i=1:m
        train_data_nml(i,:) = (train_data(i,:) - train_mean) ./ train_std;
    end
    % Normalize the new_data
    [new_m,new_n] = size(new_data);
    new_data_nml = zeros(new_m, new_n);
    for r=1:new_m
        new_data_nml(r,:) = (new_data(r,:) - train_mean) ./ train_std;
    end
    % Calculate the L2 distance of new_data
    
    knn_output = zeros(size(new_data,1));
    for r=1:new_m
        % For each row of new_data, calculate distance from each row of
        % train_data and get the top k classes and get the class with
        % maximum occurance.
        ip = new_data_nml(r,:);
        class_distances = zeros(m,2);
        for train_r = 1:m
            l2_distance_sqr = sum((train_data_nml(train_r,:) - ip) .^ 2);
            class_distances(train_r,:) = [train_label(train_r,1), l2_distance_sqr];
        end
        %disp(class_distances);
        sorted_l2 = sortrows(class_distances, 2);
        %disp(sorted_l2);
        top_k = sorted_l2(1:K,:);
        %disp(top_k);
        label = mode(top_k(:,1));
        %disp(label);
        knn_output(r,:) = label;
    end
    %new_accu = size(find(new_predicted == new_label),1)./size(new_label,1);
end