% function f = epanechiknov_kernel(x,h)
% f = [];
% constant = 3/4;
% 
% for xi = x.'
%     u = ((x-xi)./h);
%     u = u.^2;
%     K = constant * (1-u);
%     K = max(0, K);
%     Ksum = sum(K)/(length(x)*h);
%     f = [f; Ksum];
% end
% end

function [f] = epanechiknov_kernel(x,h)
f = zeros(size(x));
row = 1;
    for xi = transpose(x)
        u = ((x-xi)./h).^2;
        K = max(0,3 * (1-u)/4);
        K_gross = sum(K)/(length(x)*h);
        f(row, 1) = K_gross;
        row = row + 1;
    end
end