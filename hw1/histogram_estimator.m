function f = histogram_estimator(x, h)
    min_val = min(x);
    val_range = max(x) - min(x);
    n_bins = ceil((val_range+1)/h);
    bins = zeros(n_bins,1);
    f = zeros(size(x));
    % Get the bin counter
    for xi = transpose(x)
        i = floor((xi - min_val)/h) + 1;
        bins(i,1) = bins(i,1)+1;
    end
    
    % Get the final f value
    f=zeros(size(x));
    row=1;
    for xi = transpose(x)
        bin_no = floor((xi - min_val)/h) + 1;
        bin_val = bins(bin_no,1)/length(x);
        bin_val = bin_val/h;
        f(row,1) = bin_val;
        row = row + 1;
    end
end