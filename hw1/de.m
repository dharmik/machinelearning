function [] = de()
    close all;
    clear all;
    load hw1progde
    % Select different values of h
    h_values = [0.05,0.1,0.15,0.2,0.25];


    for h = h_values
        x_tr_sort = sort(x_tr);
        f1 = gaussian_kernel(x_tr_sort, h);
        figure(50)
        plot(x_tr_sort, f1, 'b')
        hold on

        f2 = epanechnikov_kernel(x_tr_sort, h);
        figure(51)
        plot(x_tr_sort, f2,'r')
        hold on

        f3 = histogram_estimator(x_tr_sort, h);
        figure(52)
        plot(x_tr_sort, f3,'g')
        hold on
    end
end
