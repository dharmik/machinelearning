function [] = de_variance()
    load hw1progde
    load variance_data
    h_values = [0.05,0.1,0.15,0.2,0.25];
    y = 0;
     a = [500,1];
    i = 1;

    new_matrix = zeros(19,500);
    for i = 1:19
        new_matrix(i,:) = randsample(x_te,500);
    end
    i = 1;
    x_data = zeros(1,5);
    var_error = zeros(1,5);
    var_gausian = zeros(1,5);
    var_epanchnikov = zeros(1,5);
    var_histgram = zeros(1,5);
    var_gausian = var_epanchnikov;
    var_histgram= var_gausian;
    for bandwidth = 0.05:0.05:0.25
    toterror = 0;
    for h = 0:(1/50):1
        data = zeros(1, 19);
        for l = 1:19
            y = 0;
            for i = 1:500
            y = y + exp(-((h-new_matrix(l,i))/bandwidth)^2/2)/(a(1)*bandwidth*sqrt(2*pi));
            end
            data(l) = y;
        end
       for l = 1:19
           toterror = toterror + (data(l)-mean2(data))^2;
       end
    end
       toterror = toterror/19;
       x_data(i) = bandwidth;
       variance_error(i) = toterror;
       i = i+1;
    end
    %plot(x_data,variance_error);
    figure(9)
    plot(h_values, var_gaussian,':')
    hold on
    figure(9)
    plot(h_values, var_epanechnikov,'--')
    hold on
    figure(9)
    plot(h_values, var_histogram)
    hold on
end