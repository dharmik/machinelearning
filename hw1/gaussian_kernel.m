% function f = gaussian_kernel(x,h)
% f = [];
% constant =(1/sqrt(2*pi));
% 
% for xi = x.'
%     u = ((x-xi)./h).^2;
%     K = constant * exp(-u./2);
%     Ksum = sum(K)/(length(x)*h);
%     f = [f; Ksum];
% end
% end

function [f] = gaussian_kernel(x,h)
f = zeros(size(x));
row = 1;
    for xi = transpose(x)
        u = ((x-xi)./h).^2;
        K = exp(-u./2)/sqrt(2*pi);
        K_gross = sum(K)/(length(x)*h);
        f(row, 1) = K_gross;
        row = row + 1;
    end
end