function [new_accu, train_accu] = naive_bayes(train_data, train_label, new_data, new_label)
% naive bayes classifier
% Input:
%  train_data: N*D matrix, each row as a sample and each column as a
%  feature
%  train_label: N*1 vector, each row as a label
%  new_data: M*D matrix, each row as a sample and each column as a
%  feature
%  new_label: M*1 vector, each row as a label
%
% Output:
%  new_accu: accuracy of classifying new_data
%  train_accu: accuracy of classifying train_data 
%
% CSCI 567: Machine Learning, Fall 2015, Homework 1

    %train_label = [1;2;1];
%train_data = [1,0,1;0,1,0;1,0,0];
%new_data=[1,0,0;0,1,0];
%new_label = [1;2];

    %Get the list of unique labels
    labels = transpose(unique(train_label));
    %disp(labels);
    
    %disp(train_data);

    % Create a matrix containing the count of 1s n 0s in that column for each label
    count_1 = zeros(size(labels,1), size(train_data,2));
    count_0 = zeros(size(labels,1), size(train_data,2));

    % Get the count of 1s in each column for a given class
    for c = labels
        count_1(c,:) = sum(train_data(train_label==c,:),1);
    end
    %disp(count_1);

    % Get the count of each label in training data
    label_count = zeros(size(transpose(labels)));

    for c = labels
        label_count(c,1) = size(find(train_label==c),1);
    end
    %disp(label_count);

    % We can get the 0s count from (label_count - count_1 )
    for c = labels
        count_0(c,:) = label_count(c,1) - count_1(c,:);
    end
    %disp(count_0);

    % substitute 0's with 0.1
    count_1(count_1 == 0) = 0.1;
    count_0(count_0 == 0) = 0.1;
    %disp(count_1);
    %disp(count_0);

    % Predict the labels from new data
    m = size(new_data,1);
    n = size(new_data,2);

    predicted_labels = zeros(size(new_label));
    for r=1:m
        % Each row of the input data
        ip1 = new_data(r,:);
        % Row where the elements are 0
        ip0 = ip1==0;
        %disp(ip1);
        %disp(ip0);

        % Get the value for each class
        max_c = -9999;
        max_val = -9999;
        for c = labels
            %disp(ip1.*count_1(c,:));
            %disp(ip0.*count_0(c,:));
            %disp(n);
            %disp(label_count(c,1));
            val = sum(log(ip1.*count_1(c,:) + ip0.*count_0(c,:))) - n.*log(label_count(c,1));
            val = val + log(label_count(c,1));
            %disp(val);
            if val > max_val
                max_val = val;
                max_c = c;
            end
        end
        predicted_labels(r,1) = max_c;
    end
    %disp(predicted_labels);
    %disp(new_label);

    new_accu = size(find(predicted_labels == new_label),1)./size(new_label,1);
    %disp(new_accu);

    % Predict the labels from new data
    m = size(train_data,1);
    n = size(train_data,2);
    predicted_labels = zeros(size(train_label));
    for r=1:m
        % Each row of the input data
        ip1 = train_data(r,:);
        % Row where the elements are 0
        ip0 = ip1==0;

        % Get the value for each class
        max_c = -9999;
        max_val = -9999;
        for c = labels
            val = sum(log(ip1.*count_1(c,:) + ip0.*count_0(c,:))) - n.*log(label_count(c,1));
            val = val + log(label_count(c,1));
            if val > max_val
                max_val = val;
                max_c = c;
            end
        end
        predicted_labels(r,1) = max_c;
    end
    train_accu = size(find(predicted_labels == train_label),1)./size(train_label,1);
    %disp(train_accu);
end





