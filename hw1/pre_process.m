function [bin_matrix, int_labels] = pre_process(file_name)
    %hw1nursery_train.txt
    file_data = readtable(file_name, 'ReadVariableNames',false);
    total_cols=size(file_data, 2);
    bin_matrix = []; %zeros(size(file_data));

    %disp(size(file_data));
    %total_cols=1;
    %disp(total_cols);
    % Get each column and map the col values to binary nos
    % skip the last column which are class labels
    for c=1:total_cols-1
        col_data = table2array(file_data(:,c));
        %disp(col_data);

        % Make sure they are strings
        if isfloat(col_data)
            tmp = col_data
            col_data = {}
            for i=1:size(tmp,1)
                col_data = [col_data;int2str(tmp(i,1))];
            end
        end

        unique_cols = unique(col_data);

        % Map col values to binary nos
        unique_keys = transpose(unique_cols);
        %disp(isfloat(unique_keys));
        unique_col_n = size(unique_cols,1);
        %disp(unique_col_n);
        vals = {};
        for i=1:unique_col_n
            tmp=[];
            for j=1:unique_col_n
                if j == i
                    tmp = ['1', tmp];
                else
                    tmp = ['0', tmp];
                end
            end
            vals = [vals, tmp];
        end
        %disp(unique_keys);
        %disp(vals);
        mapObj = containers.Map(unique_keys,vals);
        %disp(mapObj(col_data{1,1}));
        binary_data = []; %zeros(size(col_data,1),unique_col_n);
        for r=1:size(col_data,1)
            k = col_data{r,1};
            %if ~iscell(col_data) & isdouble(col_data(r,1))
            %    k = k + '0';
            %end
            binary_data(r,:) = mapObj(k);
        end
        %disp(binary_data - '0');
        bin_matrix = [bin_matrix, binary_data];
    end
    %disp(size(bin_matrix - '0'));
    bin_matrix = bin_matrix - '0';

    %%% Process class labels
    label_col = table2array(file_data(:,total_cols));

    % Convert to strings if they are not strings
     if isfloat(label_col)
        tmp = label_col
        label_col = {}
        for i=1:size(tmp,1)
            label_col = [label_col;int2str(tmp(i,1))];
        end
     end

    unique_labels = unique(label_col);
    label_counter = 1;
    % Create a map object to store the int labels for the classes
    label_map = containers.Map;
    for i=1:size(unique_labels,1)
        label_map(unique_labels{i,1}) = label_counter;
        label_counter = label_counter + 1;
    end

    % Create a column matrix with new class labels.
    int_labels = zeros(size(label_col));

    for i=1:size(label_col,1)
        old_label = label_col{i,1};
        int_labels(i,1) = label_map(old_label);
    end
    %disp(unique_labels);
    %for i=1:size(unique_labels,1)
    %    disp(label_map(unique_labels{i,1}));
    %end
    %disp(size(label_col,1));
    %disp(size(int_labels));
end

