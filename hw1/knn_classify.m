function [new_accu, train_accu] = knn_classify(train_data, train_label, new_data, new_label, K)
% k-nearest neighbor classifier
% Input:
%  train_data: N*D matrix, each row as a sample and each column as a
%  feature
%  train_label: N*1 vector, each row as a label
%  new_data: M*D matrix, each row as a sample and each column as a
%  feature
%  new_label: M*1 vector, each row as a label
%  K: number of nearest neighbors
%
% Output:
%  new_accu: accuracy of classifying new_data
%  train_accu: accuracy of classifying train_data (using leave-one-out
%  strategy)
%
% CSCI 567: Machine Learning, Fall 2015, Homework 1
%train_data= [1,0;3,2;0,1;5,4;2,3;4,5];
%train_label=[1;2;1;3;2;3];
%new_data=[1,1;3,3;5,5];
%new_label=[1;2;3];
% disp(size(train_data));
% disp(size(train_label));
% disp(size(new_data));
% disp(size(new_label));
% disp(K);
    % Normalise the training data
    [m,n] = size(train_data);
    train_mean = mean(train_data);
    train_std = std(train_data);
    %disp(train_mean);
    train_data_nml = zeros(m,n);
    for i=1:m
        train_data_nml(i,:) = (train_data(i,:) - train_mean) ./ train_std;
    end
    %disp(train_data_nml);
    
    % Normalize the new_data
    [new_m,new_n] = size(new_data);
    new_data_nml = zeros(new_m, new_n);
    for r=1:new_m
        new_data_nml(r,:) = (new_data(r,:) - train_mean) ./ train_std;
    end
    %disp(new_data_nml);
    % Calculate the L2 distance of new_data
    
    new_predicted = zeros(size(new_label));
    for r=1:new_m
        % For each row of new_data, calculate distance from each row of
        % train_data and get the top k classes and get the class with
        % maximum occurance.
        ip = new_data_nml(r,:);
        class_distances = zeros(m,2);
        for train_r = 1:m
            l2_distance_sqr = sum((train_data_nml(train_r,:) - ip) .^ 2);
            class_distances(train_r,:) = [train_label(train_r,1), l2_distance_sqr];
        end
        %disp(class_distances);
        sorted_l2 = sortrows(class_distances, 2);
        %disp(sorted_l2);
        top_k = sorted_l2(1:K,:);
        %disp(top_k);
        label = mode(top_k(:,1));
        %disp(label);
        new_predicted(r,:) = label;
    end
    new_accu = size(find(new_predicted == new_label),1)./size(new_label,1);
    %disp(new_accu);
    % Finding th accuracy of train_data using leave one out
    correct = 0;

    for r =1:m
        test_row_nml = train_data_nml(r,:); 
        test_row_label = train_label(r,1);
        new_train_data_nml = train_data_nml;
        new_train_label = train_label;
        new_train_data_nml(r,:) = [];
        new_train_label(r,:) = [];
        r1 = size(new_train_data_nml,1);

        class_distances = zeros(r1,2);
        for train_r = 1:r1
            l2_distance_sqr = sum((new_train_data_nml(train_r,:) - test_row_nml) .^ 2);
            class_distances(train_r,:) = [new_train_label(train_r,1), l2_distance_sqr];
        end
        sorted_l2 = sortrows(class_distances, 2);
        top_k = sorted_l2(1:K,:);
        label = mode(top_k(:,1));
        %disp(test_row);
        %disp(label);
        if label == test_row_label
            correct = correct + 1;
        end   
    end
    train_accu = correct /m;
    %disp(train_accu);
end

%td= [1,0;3,2;0,1;5,4;2,3;4,5];
%tl=[1;2;1;3;2;3];
%nd=[1,1;3,3;5,5];
%nl=[1;2;3];
%knn_classify(td,tl,nd,nl,3);
