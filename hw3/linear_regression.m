function [] = linear_regression()
    lambda = [0, 10^-4, 10^-3, 10^-2, 10^-1,1,100, 1000];
    avg_test_err = 0;
    for run = 1:3
        [train_data, test_data] = get_processed_data();
        train_rows = size(train_data,1);
        split_size = fix(train_rows/5);
        opt_lambda = 0;
        min_err = 99999;
        for l = lambda
            start_row = 1;
            count = 0;
            err = 0;
            for k=1:5
                % Get 1/5 of train data as dev data
                end_row = min([start_row + split_size, train_rows]);
                if start_row > end_row
                    break
                end
                count = count+1;
                d_data=train_data(start_row:end_row, :);
                t_data = train_data;
                t_data(start_row:end_row, :) = [];
                %[start_row  end_row]
                start_row = end_row + 1;

                y_t_data = t_data(:,1);
                t_data(:,1) = 1;
                y_d_data = d_data(:,1);
                d_data(:,1) = 1;
                w = (inv(t_data'*t_data + l.*eye(size(t_data,2)))) * t_data'*y_t_data;
                x = sum(((d_data*w) - y_d_data).^2)/size(y_d_data,1);
                err = err + x;
                %fprintf ('Linear Regression run=%d lambda=%f k=%d err=%f\n',run,l,k,x);
            end
            avg_err = err/count;
            %fprintf ('Linear Regression run=%d lambda=%f avg_err=%f\n',run,l,avg_err);
            if min_err > avg_err
                min_err = avg_err;
                opt_lambda = l;
            end
        end
        fprintf ('Linear Regression run=%d opt_lambda=%f\n',run,opt_lambda);
        % Now test the err on test data using optimal lambda
        y_test = test_data(:,1);
        test_data(:,1) = 1;
        y_train = train_data(:,1);
        train_data(:,1) = 1;
        w=(inv(train_data'*train_data + opt_lambda*eye(size(train_data,2)))) * train_data'*y_train;
        err = sum(((test_data*w) - y_test).^2)/size(y_test,1);
        %fprintf ('Linear Regression run=%d lambda=%f test_err=%f\n',run,opt_lambda,err);
        avg_test_err = avg_test_err+err;
    end
    avg_test_err = avg_test_err/3;
    fprintf ('Linear Regression avg_test_err=%f\n',avg_test_err);
end