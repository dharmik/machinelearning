function [] = CSCI567_hw3_fall15()
    % Bias-Variance Tradeoff
    disp('Bias-Variance Regression');
    bias_variance();
    
    %regression();
    %Q6
    disp('NOTE: Using 50/50 training/test splits and 3 runs');
    disp('Linear Ridge Regression');
    linear_regression();
    disp('Kernel Ridge Regression');
    kernel_ridge_regression();
end