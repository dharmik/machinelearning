function [] = kernel_ridge_regression()
    warning('off','all');
    lambda = [0, 10^-4, 10^-3, 10^-2, 10^-1,1,10,100, 1000];
    total_test_err = 0;
    for rand_split=1:3
        fprintf('Run=%d\n',rand_split);
        [train_data, test_data] = get_processed_data();
        total_test_err = total_test_err + linear_kernel(train_data, test_data, lambda);
    end
    fprintf('Linear Ridge Kernel Avg Test Error=%f\n',total_test_err/3);
    total_test_err = 0;
    
    for rand_split=1:3
        fprintf('Run=%d\n',rand_split);
        [train_data, test_data] = get_processed_data();
        total_test_err = total_test_err + polynomial_kernel(train_data, test_data, lambda);
    end
    fprintf('Polynomial Kernel Avg Test Error=%f\n',total_test_err/3);
    total_test_err = 0;
    
    for rand_split=1:3
        fprintf('Run=%d\n',rand_split);
        [train_data, test_data] = get_processed_data();
        total_test_err = total_test_err + guassian_kernel(train_data, test_data, lambda);
    end
    fprintf('Guassian Avg Test Error=%f\n',total_test_err/3);
end

function [test_err] = linear_kernel(train_data, test_data,  lambda)
    train_rows = size(train_data,1);
    split_size = fix(train_rows/5);
    min_err = 999999;
    %train_rows
    for l = lambda
        start_row = 1;
        count = 0;
        avg_err = 0;
        for i = 1:5
           % Get 1/5 of train data as dev data
           end_row = min([start_row + split_size, train_rows]);
           if start_row > end_row
               break
           end
           count = count+1;
           d_data=train_data(start_row:end_row, :);
           t_data = train_data;
           t_data(start_row:end_row, :) = [];
           start_row = end_row + 1;

           y_t_data = t_data(:,1);
           t_data(:,1) = 1;
           y_d_data = d_data(:,1);
           d_data(:,1) = 1;
           p=inv((t_data*t_data') + l*eye(size(t_data,1)));
           tmp_mat = y_t_data'*p*t_data;
           error = 0;
           for q=1:size(d_data,1)
               error = error + ((tmp_mat*d_data(q,:)') - y_d_data(q,1)).^2;
           end
           error = error/size(y_d_data,1);
           %fprintf ('Linear Ridge Regression lambda=%f k=%d k_err=%f\n',l,i,error);
           avg_err = avg_err + error;
        end
        avg_err = avg_err/count;
        %fprintf ('Linear Ridge Regression lambda=%f k_avg_err=%f\n',l,avg_err);
        % compre avg err to min_err and update lambda
        if avg_err < min_err
            min_err = avg_err;
            opt_lambda = l;
        end
    end
    fprintf('Linear kernel optimum values: lambda=%f\n',opt_lambda);
    % Test data
    y_test = test_data(:,1);
    test_data(:,1) = 1;
    train = train_data;
    y_train = train(:,1);
    train(:,1) = 1;
    p=inv((train*train')+opt_lambda*eye(size(train,1)));
    tmp_matrix = y_train'*p*train;
    error = 0;
    for q=1:size(test_data,1)
       error = error + ((tmp_matrix*test_data(q,:)') - y_test(q,1)).^2;
    end
    test_err = error/size(test_data,1);
    %fprintf ('Linear Ridge Regression lambda=%f test_err=%f\n',opt_lambda,test_err);
end

function [test_err] = polynomial_kernel(train_data, test_data,  lambda)
    c = [1,2,3,4];
    a = [-1,-0.5,0,0.5,1];
    train_rows = size(train_data,1);
    split_size = fix(train_rows/5);
    min_err = 999999;
    opt_c = 0;
    opt_a = 0;
    opt_lambda = 0;
    %train_rows
    for l = lambda
        for a1=a
            for c1=c
                start_row = 1;
                count = 0;
                avg_err = 0;
                for i = 1:5
                   % Get 1/5 of train data as dev data
                   end_row = min([start_row + split_size, train_rows]);
                   if start_row > end_row
                       break
                   end
                   count = count+1;
                   d_data=train_data(start_row:end_row, :);
                   t_data = train_data;
                   t_data(start_row:end_row, :) = [];
                   %[start_row  end_row]
                   start_row = end_row + 1;

                   y_t_data = t_data(:,1);
                   t_data(:,1) = 1;
                   y_d_data = d_data(:,1);
                   d_data(:,1) = 1;
                   p=inv((t_data*t_data' + a1)^c1 + l*eye(size(t_data,1)));
                   tmp_matrix = y_t_data'*p*t_data;
                   error = 0;
                   for q=1:size(d_data,1)
                       error = error + ((tmp_matrix*d_data(q,:)'+a1).^c1 - y_d_data(q,1)).^2;
                   end
                   error = error/size(y_d_data,1);
                   %fprintf ('Polynomial KR lambda=%f a=%f c=%f k=%d k_err=%f\n',l,a1,c1,i,error);
                   avg_err = avg_err + error;
                end
                avg_err = avg_err/count;
                %fprintf ('Polynomial KR lambda=%f a=%f c=%f avg_err=%f\n',l,a1,c1,avg_err);
                % compre avg err to min_err and update lambda
                if avg_err < min_err
                    min_err = avg_err;
                    opt_lambda = l;
                    opt_a = a1;
                    opt_c = c1;
                end
            end
        end
    end
    fprintf ('Polynomial KR optimum lambda=%f a=%f c=%f\n',opt_lambda,opt_a,opt_c);
    % Test data
    y_test = test_data(:,1);
    test_data(:,1) = 1;
    train = train_data;
    y_train = train(:,1);
    train(:,1) = 1;
    p=inv((train*train' + opt_a)^opt_c + opt_lambda*eye(size(train,1)));
    tmp_matrix = y_train'*p*train;
    error = 0;
    for q=1:size(test_data,1)
       error = error + ((tmp_matrix*test_data(q,:)') - y_test(q,1)).^2;
    end
    test_err = error/size(test_data,1);
    %fprintf ('Polynomial KR optimum lambda=%f a=%f c=%f test_err=%f\n',opt_lambda,opt_a,opt_c,test_err);
end

function [test_err] = guassian_kernel(train_data, test_data, lambda)
    sigma = [0.125, 0.25, 0.5, 1,2,4,8];
    train_rows = size(train_data,1);
    split_size = fix(train_rows/5);
    min_err = 999999;
    opt_lambda = 0;
    opt_sigma = 0;
    %train_rows
    for l = lambda
        for v=sigma
            start_row = 1;
            count = 0;
            avg_err =0;
            for i = 1:5
               % Get 1/5 of train data as dev data
               end_row = min([start_row + split_size, train_rows]);
               if start_row > end_row
                   break
               end
               count = count+1;
               d_data=train_data(start_row:end_row, :);
               t_data = train_data;
               t_data(start_row:end_row, :) = [];
               start_row = end_row + 1;

               y_t_data = t_data(:,1);
               t_data(:,1) = 1;
               y_d_data = d_data(:,1);
               d_data(:,1) = 1;
               g=exp(-squareform(pdist(t_data,'euclidean')) ./v);
               p = inv(g + l*eye(size(t_data,1)));
               tmp_matrix = y_t_data'*p;
               error = 0;
               for q=1:size(d_data,1)
                   error = error + (tmp_matrix*(exp(-sqrt(sum((t_data - repmat(d_data(q,:),size(t_data,1),1)).^2,2)) ./ v)) - y_d_data(q,1)).^2;
               end
               error = error/size(y_d_data,1);
               %fprintf('Guassian kernel lambda=%f sigma=%f err=%f k=%d\n',l, v, error,i);
               avg_err = avg_err + error;
            end
            avg_err = avg_err/count;
            % compare avg err to min_err and update lambda
            if avg_err < min_err
                min_err = avg_err;
                opt_lambda = l;
                opt_sigma = v;
            end
            %fprintf('Guassian kernel lambda=%f sigma=%f err=%f\n',l, v, avg_err);
        end
    end
    fprintf('Guassian kernel optimum values: lambda=%f sigma=%f\n',opt_lambda, opt_sigma);
    % Test data
    y_test = test_data(:,1);
    test_data(:,1) = 1;
    train = train_data;
    y_train = train(:,1);
    train(:,1) = 1;
    error = 0;
    g=exp(-squareform(pdist(train,'euclidean')) ./v);
    p = inv(g + opt_lambda*eye(size(train,1)));
    tmp_matrix = y_train'*p;
    for q=1:size(test_data,1)
       error = error + (tmp_matrix*(exp(-sqrt(sum((train - repmat(test_data(q,:),size(train,1),1)).^2,2)) ./ opt_sigma)) - y_test(q,1)).^2;
    end
    test_err = error/size(test_data,1);
    %fprintf('Guassian kernel optimum values: lambda=%f sigma=%f test_err=%f\n',opt_lambda, opt_sigma,test_err);
end
