function[train_data, test_data] = get_processed_data()
    file_name = 'space_ga.txt';
    data_raw = dlmread(file_name);
    data_random_raw = data_raw(randperm(size(data_raw,1)),:);
    train_rows = fix(0.5*size(data_random_raw,1));
    train_data_raw = data_random_raw(1:train_rows,:);
    % Normalize train_data
    train_data = zeros(size(train_data_raw));
    train_mean = nanmean(train_data_raw);
    train_std = nanstd(train_data_raw);
    for i=1:size(train_data_raw,2)
        train_data(:,i)= (train_data_raw(:,i) - train_mean(:,i)) ./ train_std(:,i);
    end
    test_data_raw = data_random_raw(train_rows+1:size(data_random_raw,1),:);
    %Normalize test data
    test_data = zeros(size(test_data_raw));
    for i=1:size(test_data_raw,2)
        test_data(:,i)= (test_data_raw(:,i) - train_mean(:,i)) ./ train_std(:,i);
    end
end