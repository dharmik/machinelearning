function [] = bias_variance()
    bias_variance2(10);
    [X,Y] = bias_variance2(100);
    bias_variance_reg(X,Y,100);
end

function [] = bias_variance_reg(X, Y, rows)
    lambda = [0.01,0.1,1,10];
    b_gross = zeros(3,100);
    b_sqr_sum = zeros(rows,100);
    for l=lambda
        for i=1:100
            X_data = [ones(rows,1) X(:,i) X(:,i).^2];
            b_gross(:,i) = ridge(Y(:,i),X_data,l);
            b_sqr_sum(:,i) = sum(b_gross(:,i).^2);
        end
        % Calculate bais
        bias = compute_bias_l(X,Y,b_gross,l,b_sqr_sum,3);
        % Calculate variance
        var = compute_variance_l(X,Y,b_gross,l,b_sqr_sum,3);
        fprintf('h(x) lambda=%f bias=%f variance=%f\n', l,bias,var);
    end
end

function [bias] = compute_bias_l(X,Y,b,l,b_sum,f)
    X_data = zeros(size(X,1),f);
    mean_bias = zeros(1,100);
    for i=1:100
        x_data_i = X(:,i);
        for j=1:f
            X_data(:,j) = x_data_i .^ (j-1);
        end
        tmp_mat = X_data * b + l*b_sum;
        pred_vals = mean(tmp_mat,2);
        mean_bias(1,i)= sum((pred_vals-Y(:,i)).^2)/size(X,1);
    end
    bias = mean(mean_bias);    
end


function [var] = compute_variance_l(X, Y, b, l, b_sum, f)
    X_data = zeros(size(X,1),f);
    Y_pred = zeros(size(X,1),100);
    for i=1:100
        x_data_i = X(:,i);
        for j=1:f
            X_data(:,j) = x_data_i .^ (j-1);
        end
        pred_vals = X_data * b + l*b_sum;
        mean_pred_vals = mean(pred_vals,2);
        Y_pred(:,i) = (pred_vals(:,i) - mean_pred_vals) .^ 2;
    end
    var = mean(mean(Y_pred));
end

function [X_gross, Y_gross] = bias_variance2(size)
    mean_sq_err = zeros(6,100);
    e = 0.05;
    %size = 10;
    X_gross = zeros(size, 100);
    Y_gross = zeros(size, 100);
    b1_gross = ones(1,100);
    b2_gross = zeros(1,100);
    b3_gross = zeros(2, 100);
    b4_gross = zeros(3, 100);
    b5_gross = zeros(4, 100);
    b6_gross = zeros(5, 100);
    for i = 1:100 % Change to 100 later
        X_rand = 2* rand(size,1) -1;
        y = 2.*X_rand.*X_rand;
        X_gross(:,i)= X_rand;
        Y_gross(:,i) = y;
        y = y+e;
        %g1 =1
        g1 = 1;
        mean_sq_err(1,i) = sum((y - g1).^2)/size;
        
        %g2 = w0
        g2 = mean(y);
        b2_gross(1,i)=g2;
        mean_sq_err(2,i) = sum((y - g2).^2)/size;
        
        %g3 = w0 + w1x
        X_ip = [ones(size,1) X_rand];
        g3 = regress(y,X_ip);
        b3_gross(:,i)=g3;
        mean_sq_err(3,i) = sum((y - X_ip*g3).^2)/size;
        
        %g4 = w0 + w1x + w2x2
        X_ip = [ones(size,1) X_rand X_rand.^2];
        g4 = regress(y,X_ip);
        b4_gross(:,i)=g4;
        mean_sq_err(4,i) = sum((y - X_ip*g4).^2)/size;
        
        
        %g5 = w0 + w1x + w2x2 + w3x3
        X_ip = [ones(size,1) X_rand X_rand.^2 X_rand.^3];
        g5 = regress(y,X_ip);
        b5_gross(:,i)=g5;
        mean_sq_err(5,i) = sum((y - X_ip*g5).^2)/size;
        
        
        %g6 = w0 + w1x + w2x2 + w3x3 + w4x4
        X_ip = [ones(size,1) X_rand X_rand.^2 X_rand.^3 X_rand.^4];
        g6 = regress(y,X_ip);
        b6_gross(:,i)=g6;
        mean_sq_err(6,i) = sum((y - X_ip*g6).^2)/size;
    end
    %disp(mean_sq_err);
    
    % Disp histogram for each fn.
    for i=1:6
        figure(i+size);
        hist(mean_sq_err(i,:));
    end
    
    %g1
    % Calculating bias for g1, a constant function
    bias_g1 =  mean(mean((Y_gross-1).^2));
    var = compute_variance(X_gross,Y_gross,b1_gross,1);
    fprintf('g1 bias=%f variance=%f\n',bias_g1,var);
    bias = compute_bias(X_gross, Y_gross,b2_gross, 1);
    var = compute_variance(X_gross,Y_gross,b2_gross,1);
    fprintf('g2 bias=%f variance=%f\n',bias,var);
    bias = compute_bias(X_gross, Y_gross,b3_gross, 2);
    var = compute_variance(X_gross,Y_gross,b3_gross,2);
    fprintf('g3 bias=%f variance=%f\n',bias,var);
    bias = compute_bias(X_gross, Y_gross,b4_gross, 3);
    var = compute_variance(X_gross,Y_gross,b4_gross,3);
    fprintf('g4 bias=%f variance=%f\n',bias,var);
    bias = compute_bias(X_gross, Y_gross,b5_gross, 4);
    var = compute_variance(X_gross,Y_gross,b5_gross,4);
    fprintf('g5 bias=%f variance=%f\n',bias,var);
    bias = compute_bias(X_gross, Y_gross,b6_gross, 5);
    var = compute_variance(X_gross,Y_gross,b6_gross,5);
    fprintf('g6 bias=%f variance=%f\n',bias,var);
    
    %sum_matrix = get_summation(X_gross, Y_gross,b3_gross, 2)
    %bias = sum(sum((Y_gross - sum_matrix) .^ 2))/100;
    
end

function [bias] = compute_bias(X,Y,b,f)
    X_data = zeros(size(X,1),f);
    mean_bias = zeros(1,100);
    for i=1:100
        x_data_i = X(:,i);
        for j=1:f
            X_data(:,j) = x_data_i .^ (j-1);
        end
        pred_vals = mean(X_data * b,2);
        mean_bias(1,i)= sum((pred_vals-Y(:,i)).^2)/size(X,1);
    end
    bias = mean(mean_bias);    
end

function [var] = compute_variance(X, Y, b, f)
    X_data = zeros(size(X,1),f);
    Y_pred = zeros(size(X,1),100);
    for i=1:100
        x_data_i = X(:,i);
        for j=1:f
            X_data(:,j) = x_data_i .^ (j-1);
        end
        pred_vals = X_data * b;
        mean_pred_vals = mean(pred_vals,2);
        Y_pred(:,i) = (pred_vals(:,i) - mean_pred_vals) .^ 2;
    end
    var = mean(mean(Y_pred));
end