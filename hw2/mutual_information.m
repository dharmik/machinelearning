function [] = mutual_information(c_train,survival_train)
disp('Mutual Information');
% Pclass
ig = get_ig(c_train, survival_train, 1);
disp('Information gain for pclass');
disp(ig);

% sibsp
ig = get_ig(c_train, survival_train, 5);
disp('Information gain for sibsp');
disp(ig);

% parch
ig = get_ig(c_train, survival_train, 6);
disp('Information gain for parch');
disp(ig);

%age
% Dividing the age values among 10 bins with equal density would put
% same no. of values in each bin, thus probability would be 1/n_bins
ig = get_ig_bins(c_train, survival_train, 4, 10);
disp('Information gain for Age');
disp(ig);

%Fare
% Dividing the age values among 10 bins with equal density would put
% same no. of values in each bin, thus probability would be 1/n_bins
ig = get_ig_bins(c_train, survival_train, 8, 10);
disp('Information gain for Fare');
disp(ig);

end


function [ig] = get_ig_bins(c_train, survival_train, col, bins)
    total_count = size(c_train,1);
    col_train = cell2mat(c_train(:,col));
    % Remove any NaNs and replace by Max val
    col_train(isnan(col_train))= -1;
    col_sort = sort(col_train);
    bin_size = ceil(total_count/bins);
    ig = 0;
    for y=[0,1]
        p_y = size(survival_train(survival_train == y),1) / total_count;
        %p_x = 1/bins;
        prev_val = 0;
        for x=1:bins
            cur_val = col_sort(min(x*bin_size,total_count),1);
            p_x_y = size(survival_train(survival_train==y & col_train>=prev_val & col_train < cur_val), 1) / total_count;
            p_x = size(col_train(col_train>=prev_val & col_train < cur_val),1)/total_count;
            prev_val = cur_val;
            if p_x_y > 0
               ig = ig + ( p_x_y * log(p_x_y / (p_x * p_y)));
            end
        end
    end
end

function [ig] = get_ig(c_train, survival_train, col)
    total_count = size(c_train,1);
    col_train = cell2mat(c_train(:,col));
    col_unique = unique(col_train);
    % Remove NaN from unique values
    col_unique(isnan(col_unique)) = [];
    col_unique = transpose(col_unique);
    %I= Sum(x)Sum(y) p(x,y)*log(p(x,y) / p(x)*p(y))
    ig = 0;
    for y=[0,1]
       p_y = size(survival_train(survival_train == y),1) / total_count;
       for x = col_unique
           p_x = size(col_train(col_train == x),1) / total_count;
           p_x_y = size(survival_train(col_train==x & survival_train == y),1) / total_count;
           if p_x_y > 0
               ig = ig + ( p_x_y * log(p_x_y / (p_x * p_y)));
           end
       end
    end
end