function [X_train_top10f, X_test_top10f] = feature_selection(X_train_nml, X_test_nml, survived_train,survived_test)
    disp('Sequential Feature Selection');
    features = [];
    training_accuracy = zeros(1,10);
    test_accuracy = zeros(1,10);
    for i=1:10
        max_accuracy = 0;
        new_f = 0;
        initial_f = 1;
        if ~isempty(features)
            initial_f = features(length(features)) + 1;
        end
        for j=1:size(X_train_nml,2)
            f = [features j];
            b1 = glmfit(X_train_nml(:,f),survived_train);
            test_predict = glmval(b1,X_train_nml(:,f),'identity') > 0.5;
            accu = (sum(test_predict == survived_train)) ./ length(survived_train);
            if accu > max_accuracy
                max_accuracy = accu;
                new_f = j;
            end
        end
        features = [features new_f];
        training_accuracy(i) = max_accuracy;
        % Test accuracy
        test_predict = glmval(b1,X_test_nml(:,features),'identity') > 0.5;
        test_accuracy(i) = (sum(test_predict == survived_test)) ./ length(survived_test);
    end
    % Plot as a function of features.
    figure(9);
    plot([1:10],training_accuracy)
    hold on
    plot([1:10],test_accuracy, 'r')
    X_train_top10f = X_train_nml(:,features);
    X_test_top10f = X_test_nml(:,features);
end