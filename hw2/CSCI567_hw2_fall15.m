function [] = CSCI567_hw2_fall15()
warning('off','all');
% 4a - Load data
[~,~,c] = xlsread('titanic3.xls');
% Remove the last row, its all NaNs!!!
c(size(c,1),:) = [];
[total_rows,total_cols] = size(c);

nan_count = zeros(1,total_cols);

for col=1:total_cols
    count = 0;
    for row=1:total_rows
        if isnan(c{row,col})
            count = count + 1;
        end
    end
    nan_count(1,col)= count;
end
disp('Column count of NaNs')
disp(nan_count);


%Permute rows and separate test and train
% Remove the 1st header row.
c_data = c;
c_data(1,:) = [];
c_random = c_data(randperm(size(c_data,1)),:);
total_rows = size(c_random,1);
train_size = ceil(total_rows/2);
c_train = c_random(1:train_size,:);
c_test = c_random(train_size+1:total_rows,:);


%Remove survived column and separate it
survived_train = cell2mat(c_train(:,2));
c_train(:,2) = [];
survived_test = cell2mat(c_test(:,2));
c_test(:,2) = [];


% 4b - Graph between numeric independant variables and dependent variable
monotonic_relationship(c_train, survived_train);

% 4c - Mutual Information
mutual_information(c_train, survived_train);

% 4d- missing values
missing_values(c_train, c_test, survived_train, survived_test)
[X_train_nml, X_test_nml] = basis_expansion(c_train, c_test);

%4f - sequential feature selection
[X_train_top10f, X_test_top10f] = feature_selection(X_train_nml, X_test_nml, survived_train, survived_test);
gradient_descent(X_train_top10f, survived_train, X_test_top10f, survived_test);
newton(X_train_top10f, survived_train, X_test_top10f, survived_test);
end

