function [X_train_nml, X_test_nml] = basis_expansion(c_train, c_test)
    disp('Basis Expansion');
    %sex-1, pclass-2, fare-3, embarked-4,5, parch-6, sibsp-7, age-8
    X_train = format_matrix(c_train);
    X_test = format_matrix(c_test);
    [X_train2, X_test2] = get_encoded_features(X_train, X_test);
    % Append square-root of the numeric independent variables
    %(pclass, age, sibsp,parch, fare) to X matrices
    X_train2 = [X_train2 sqrt(X_train(:,2)) sqrt(X_train(:,8)) sqrt(X_train(:,7)) sqrt(X_train(:,6)) sqrt(X_train(:,3))];
    %disp(size(X_train2));
    train_cols = size(X_train2, 2);
    for j=1:train_cols
        for i = j+1:train_cols
            X_train2 = [X_train2 X_train2(:,j).* X_train2(:,i)];
        end
    end
    %disp(size(X_train2));
    
    X_train3 = [];
    features = [];
    for i=1:size(X_train2, 2)
        uniq_vals = unique(X_train2(:,i));
        uniq_vals(isnan(uniq_vals)) = [];
        if length(uniq_vals)>1
            X_train3 = [X_train3 X_train2(:,i)];
            features = [features i];
        end
    end
    % Normaize train data
    X_train_nml = zeros(size(X_train3));
    X_train_mean = nanmean(X_train3);
    X_train_std = nanstd(X_train3);
    for i=1:size(X_train3,1)
        X_train_nml(i,:)= (X_train3(i,:) - X_train_mean) ./ X_train_std;
    end

    disp('Matrix size of train data')
    disp(size(X_train3));
    
    % Append square-root of the numeric independent variables
    %(pclass, age, sibsp,parch, fare) to X matrices
    X_test2 = [X_test2 sqrt(X_test(:,2)) sqrt(X_test(:,8)) sqrt(X_test(:,7)) sqrt(X_test(:,6)) sqrt(X_test(:,3))];
    %disp(size(X_test2));
    test_cols = size(X_test2, 2);
    for j=1:test_cols
        for i = j+1:test_cols
            X_test2 = [X_test2 X_test2(:,j).* X_test2(:,i)];
        end
    end
    %disp(size(X_test2));
    
    X_test3 = X_test2(:,features);
    X_test_nml = zeros(size(X_test3));
    for i=1:size(X_test3,1)
        X_test_nml(i,:)= (X_test3(i,:) - X_train_mean) ./ X_train_std;
    end
    
    disp('Matrix size of test data')
    disp(size(X_test3));
    
end


function [X_encoded_matrix, Y_encoded_matrix] = get_encoded_features(X_matrix, Y_matrix)
    %sex-1, pclass-2, fare-3, embarked-4,5, parch-6, sibsp-7, age-8    
    sex_encoded = X_matrix(:,1);
    %size(sex_encoded)
    pclass_encoded = encode_matrix(X_matrix(:,2),2);
    %size(pclass_encoded)
    [X_fare_encoded, Y_fare_encoded] = binify(X_matrix(:,3),Y_matrix(:,3),10,9);
    %size(fare_encoded)
    embarked_encoded = X_matrix(:,4:5);
    %size(embarked_encoded)
    parch_encoded = encode_matrix(X_matrix(:,6),7);
    %size(parch_encoded)
    sibsp_encoded = encode_matrix(X_matrix(:,7),6);
    %size(sibsp_encoded)
    [X_age_encoded, Y_age_encoded] = binify(X_matrix(:,8),Y_matrix(:,8),10,9);
    %size(age_encoded)
    X_encoded_matrix = [sex_encoded pclass_encoded X_fare_encoded embarked_encoded parch_encoded sibsp_encoded X_age_encoded];
    sex_encoded = Y_matrix(:,1);
    %size(sex_encoded)
    pclass_encoded = encode_matrix(Y_matrix(:,2),2);
    %size(pclass_encoded)
    embarked_encoded = Y_matrix(:,4:5);
    %size(embarked_encoded)
    parch_encoded = encode_matrix(Y_matrix(:,6),7);
    %size(parch_encoded)
    sibsp_encoded = encode_matrix(Y_matrix(:,7),6);
    %size(sibsp_encoded)
    Y_encoded_matrix = [sex_encoded pclass_encoded Y_fare_encoded embarked_encoded parch_encoded sibsp_encoded Y_age_encoded];
end

function [X_encoded, Y_encoded] = binify(matrix, matrix_test, bins, bits)
    edges = zeros(1,bins);
    matrix_bin = zeros(size(matrix));
    matrix_bin_test = zeros(size(matrix_test));
    col_sort = sort(matrix);
    rows = size(matrix(~isnan(matrix)),1);
    bin_size = ceil(rows/bins);
    prev_val = 0;
    for x=1:bins
        cur_val = col_sort(min(x*bin_size,rows),1);
        if x == bins
            cur_val = max(matrix)+1;
        end
        edges(x) = cur_val;
        matrix_bin(matrix>=prev_val & matrix<cur_val) = x;
        matrix_bin_test(matrix_test >=prev_val & matrix_test<cur_val) = x;
        prev_val = cur_val;
    end
    matrix_bin(matrix_bin==0) = NaN;
    matrix_bin_test(matrix_bin_test==0) = NaN;
    X_encoded = encode_matrix(matrix_bin,bits);
    Y_encoded = encode_matrix(matrix_bin_test,bits);
end

function [encoded] = encode_matrix(matrix, bits)
       
    uniq_vals = transpose(unique(matrix));
    % Let Nan be as it is.
    uniq_vals(isnan(uniq_vals)) =[];
    %bits = size(uniq_vals,2) - 1;
    encoded = zeros(size(matrix,1),bits); 
    encoded(isnan(matrix),:) = NaN;
    
    
    index = bits;
    for v = uniq_vals(1,2:size(uniq_vals,2))
       encoded(matrix == v, index) = 1;
       index = index - 1;
    end
end