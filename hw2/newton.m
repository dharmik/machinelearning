function [] = newton (X_train, survival_train, X_test, survival_test)
    disp('Logistic Regression using Newton Method');
    survival_train2 = survival_train(~any(isnan(X_train),2),:);
    X_train2 = X_train(~any(isnan(X_train),2),:);
    survival_test2 = survival_test(~any(isnan(X_test),2),:);
    X_test2 = X_test(~any(isnan(X_test),2),:);
    
    for iter=1:15 %5:50
        [train_accu, test_accu] = lr_newton(X_train2, survival_train2, X_test2, survival_test2, iter);
        fprintf('iter=%d train=%f test=%f\n',iter, train_accu,test_accu);
    end
end

% Logistic regression using Newton method
function[train_accu, test_accu] = lr_newton(X_train, survival_train, X_test, survival_test, iter)
    X = [ones(size(X_train,1),1) X_train];
    [m,n] = size(X);
    theta = zeros(n, 1);
    
    for k=1:iter
        delta = ((sigmoid( X*theta) - survival_train)' * X)';
        H = X' * diag(sigmoid(X*theta)) * diag(1 - sigmoid(X*theta)) * X;
        theta = theta - inv(H') * delta;
    end
    pred_train=zeros(m,1);
    for i=1:m
        pred_train(i,1) = round(sigmoid(sum(theta' .* X(i,:))));        
    end
    train_accu = sum(pred_train == survival_train) / m;
    
    % Append 1 to test.
    X_test = [ones(size(X_test,1),1) X_test];
    [m,n] = size(X_test);
    pred_test=zeros(m,1);
    for i=1:m
        pred_test(i,1) = round(sigmoid(sum(theta' .* X_test(i,:))));
    end
    test_accu = sum(pred_test == survival_test) / m;
end

function [g] = sigmoid (z)
    g = 1 ./ (1 + exp(-1 * z));
end