function [] = missing_values(c_train, c_test, survived_train, survived_test)
    disp('Missing Values');
    X_train = format_matrix(c_train);
    X_test = format_matrix(c_test);
    
    % Normalize
    X_train_nml = zeros(size(X_train));
    X_train_mean = nanmean(X_train);
    X_train_std = nanstd(X_train);
    for i=1:size(X_train,1)
        X_train_nml(i,:)= (X_train(i,:) - X_train_mean) ./ X_train_std;
    end 
    %X_train_nml
    %survived_train
    
    % Normalize test
    X_test_nml = zeros(size(X_test));
    for i=1:size(X_test,1)
        X_test_nml(i,:)= (X_test(i,:) - X_train_mean) ./ X_train_std;
    end
    % training with age column.
    b1 = glmfit(X_train_nml,survived_train);
    % training without age column.
    X_train_nml_2 = X_train_nml(:,1:7);
    b2 = glmfit(X_train_nml_2,survived_train);
    
    %Calculation accuracy for train.
    % Train data having age value
    train_correct=0;
    age_train = X_train_nml(:,8);
    %sum(~isnan(age_train))
    %sum(isnan(age_train))
    
    X_train_age = X_train_nml(~(isnan(age_train)),:);
    test_predict = glmval(b1,X_train_age,'identity') > 0.5;
    train_correct = train_correct + sum(test_predict==survived_train(~(isnan(age_train))));
    % Train data without age values
    X_train_nanage = X_train_nml(isnan(age_train),1:7);
    test_predict = glmval(b2,X_train_nanage,'identity') > 0.5;
    train_correct = train_correct + sum(test_predict==survived_train(isnan(age_train)));
    disp('Train accuracy for multiple models');
    disp(train_correct / length(survived_train));
    
    
    %Calculation accuracy for test.
    % Test data having age value
    test_correct=0;
    age_test = X_test_nml(:,8);
    %sum(~isnan(age_test))
    %sum(isnan(age_test))
    X_test_age = X_test_nml(~(isnan(age_test)),:);
    test_predict = glmval(b1,X_test_age,'identity') > 0.5;
    test_correct = test_correct + sum(test_predict==survived_test(~(isnan(age_test))));
    % Test data without age values
    X_test_nanage = X_test_nml(isnan(age_test),1:7);
    test_predict = glmval(b2,X_test_nanage,'identity') > 0.5;
    test_correct = test_correct + sum(test_predict==survived_test(isnan(age_test)));
    disp('Test accuracy for multiple models');
    disp(test_correct / length(survived_test));
    
    %%%%%%%%% Substitution model
    X_train_sm = X_train;
    X_train_sm(isnan(X_train)) = nanmean(X_train(:,8));
    X_test_sm = X_test;
    X_test_sm(isnan(X_test)) = nanmean(X_train(:,8));
    %sum(isnan(X_test(:,8)))
    %sum(sum(isnan(X_test)))
    
    % Normalize
    X_train_sm_nml = zeros(size(X_train_sm));
    X_train_sm_mean = nanmean(X_train_sm);
    X_train_sm_std = nanstd(X_train_sm);
    for i=1:size(X_train_sm,1)
        X_train_sm_nml(i,:)= (X_train_sm(i,:) - X_train_sm_mean) ./ X_train_sm_std;
    end 
    
    % Normalize test
    X_test_sm_nml = zeros(size(X_test_sm));
    for i=1:size(X_test_sm,1)
        X_test_sm_nml(i,:)= (X_test_sm(i,:) - X_train_sm_mean) ./ X_train_sm_std;
    end
    
    % Generate model
    b_sm = glmfit(X_train_sm_nml,survived_train);
    % Train accuracy
    test_predict = glmval(b_sm,X_train_sm_nml,'identity') > 0.5;
    train_sm_correct = sum(test_predict==survived_train);
    disp('Train accuracy using substitution model');
    disp(train_sm_correct/length(survived_train));
    
    % Test accuracy
    test_predict = glmval(b_sm,X_test_sm_nml,'identity') > 0.5;
    test_sm_correct = sum(test_predict==survived_test);
    disp('Test accuracy using substitution model');
    disp(test_sm_correct/length(survived_test));
    
end