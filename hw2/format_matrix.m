function [X_mat] = format_matrix(c_matrix)
% sex, pclass, fare, embarked, parch, sibsp, age

% Go through each cell and encode
% male=0, female=1
X_mat = [];
m = size(c_matrix,1);
s_raw = c_matrix(:,3);
sex_fmt = zeros(m,1);
for i=1:m
   if strcmp(s_raw(i,1),'female')
       sex_fmt(i,1) = 1;
   else
       sex_fmt(i,1) = 0;
   end
end

X_mat = [X_mat, sex_fmt];

% pclass
X_mat = [X_mat, cell2mat(c_matrix(:,1))];

%fare
fare_fmt = cell2mat(c_matrix(:,8));
% replace Nan with avg of fare.
fare_fmt(isnan(fare_fmt)) = 33.2955;
X_mat = [X_mat, fare_fmt];

% embarked
% Go through each cell and encode
% 'C'=00, 'Q'=01 and 'S'=10
m = size(c_matrix,1);
emb_raw = c_matrix(:,10);
emb_fmt = zeros(m,2);
for i=1:m
    if strcmp(emb_raw(i,1),'C')
       emb_fmt(i,:) = [0 0];
   elseif strcmp(emb_raw(i,1),'Q')
       emb_fmt(i, :) = [0 1];
   else
       emb_fmt(i, :) = [1 0];
   end
end

X_mat = [X_mat, emb_fmt];

% parch
X_mat = [X_mat, cell2mat(c_matrix(:,6))];

% sibsp
X_mat = [X_mat, cell2mat(c_matrix(:,5))];

% age
X_mat = [X_mat, cell2mat(c_matrix(:,4))];

end