function [] = monotonic_relationship(c_train, survived_train)
disp('Monotonic Relationship');
% Part-B
% pclass
pclass_train = cell2mat(c_train(:,1));
survival_pclass = zeros(1,3);
for i= [1,2,3]
    survival_pclass(1,i) = sum(survived_train(pclass_train==i)) / length(survived_train);
end
figure(1);
bar(survival_pclass);

% sibsp
sibsp_train = cell2mat(c_train(:,5));
sibsp_unique = unique(sibsp_train);
% Remove NaN from unique values
sibsp_unique(isnan(sibsp_unique)) = [];
sibsp_unique = transpose(sibsp_unique);
survival_sibsp = zeros(1,size(sibsp_unique,2));
col = 1;
for i= sibsp_unique
    survival_sibsp(1,col) = sum(survived_train(sibsp_train==i)) / length(survived_train);
    col = col + 1;
end
figure(2);
bar(sibsp_unique, survival_sibsp);

% parch
parch_train = cell2mat(c_train(:,6));
parch_unique = unique(parch_train);
% Remove NaN from unique values
parch_unique(isnan(parch_unique)) = [];
parch_unique = transpose(parch_unique);
survival_parch = zeros(1,size(parch_unique,2));
col = 1;
for i= parch_unique
    survival_parch(1,col) = sum(survived_train(parch_train==i)) / length(survived_train);
    col = col + 1;
end
figure(3);
bar(parch_unique, survival_parch);

% Age
age_train = transpose(cell2mat(c_train(:,4)));
bin_width = ((max(age_train) - min(age_train)) + 1)/10;
survival_age = zeros(1,10);
l = min(age_train);
for i= 1:10
    survival_age(1,i) = sum(survived_train(age_train >= l & age_train < (l+bin_width))==1)/length(survived_train);
    l=l+bin_width;
end
figure(4);
bar(survival_age);

% Fare
fare_train = transpose(cell2mat(c_train(:,8)));
bin_width = ((max(fare_train) - min(fare_train)) + 1)/10;
survival_fare = zeros(1,10);
l = min(fare_train);
for i= 1:10
    survival_fare(1,i) = sum(survived_train(fare_train >= l & fare_train < (l+bin_width))==1)/length(survived_train);;
    l=l+bin_width;
end
figure(5);
bar(survival_fare);


end