function [] = gradient_descent (X_train, survived_train, X_test, survived_test)
    disp('Batch Gradient Descent');
    % Remove Nans
    survived_train2 = survived_train(~any(isnan(X_train),2),:);
    X_train2 = X_train(~any(isnan(X_train),2),:);
    survived_test2 = survived_test(~any(isnan(X_test),2),:);
    X_test2 = X_test(~any(isnan(X_test),2),:);
    alpha = 0.9;
    alpha=0.5;
    train_accu_iter = zeros(1,10);
    test_accu_iter = zeros(1,10);
    for alpha = 0.4:0.1:0.9
        for iter=1:10 %5:50
            [train_accu, test_accu] = batch_gradient_descent(X_train2, survived_train2, X_test2, survived_test2, iter, alpha);
            fprintf('Alpha=%f iter=%d train=%f test=%f\n',alpha, iter, train_accu,test_accu);
            train_accu_iter(iter) = train_accu;
            test_accu_iter(iter) = test_accu;
        end
    end
    % Accuracy using GLMFIT
    b1 = glmfit(X_train2,survived_train2);
    test_predict = glmval(b1,X_train2,'identity') > 0.5;
    train_accu_glm = (sum(test_predict == survived_train)) ./ length(survived_train);
    test_predict = glmval(b1,X_test2,'identity') > 0.5;
    test_accu_glm = (sum(test_predict == survived_test)) ./ length(survived_test);
    train_accuracy_glm = zeros(10);
    train_accuracy_glm(1:10) = train_accu_glm;
    test_accuracy_glm = zeros(10);
    test_accuracy_glm(1:10) = test_accu_glm;
    fprintf('Accuracy using GLM. Train=%f Test=%f\n',train_accu_glm, test_accu_glm);
%     figure(9);
%     plot([1,2,3,4,5,6,7,8,9,10],train_accu_iter);
%     figure(10);
%     plot([1,2,3,4,5,6,7,8,9,10],test_accu_iter);
%     figure(11);
%     plot([1,2,3,4,5,6,7,8,9,10],train_accuracy_glm);
%     figure(12);
%     plot([1,2,3,4,5,6,7,8,9,10],test_accuracy_glm);
end

function [train_accu, test_accu] = batch_gradient_descent(X_train, survived_train, X_test, survived_test, iter, alpha)
    % Append 1 column to X_train
    X = [ones(size(X_train,1),1) X_train];
    [m,n] = size(X);
    theta = zeros(1, size(X,2));

    for k=1:iter
        diffs = zeros(m,1);
        for i=1:m
            h = sigmoid(sum(theta .* X(i,:)));
            diffs(i) = survived_train(i,1) - h;
        end

        for j = 1:n
            s=sum(diffs .* X(:,j));
            theta(1,j) = theta(1,j) + s*alpha;
        end
    end
    pred_train=zeros(m,1);
    for i=1:m
        pred_train(i,1) = round(sigmoid(sum(theta .* X(i,:))));        
    end
    %pred_train
    train_accu = sum(pred_train == survived_train) / m;
    % Append 1 to test.
    X_test = [ones(size(X_test,1),1) X_test];
    [m,n] = size(X_test);
    pred_test=zeros(m,1);
    for i=1:m
        pred_test(i,1) = round(sigmoid(sum(theta .* X_test(i,:))));
    end
    %pred_test
    test_accu = sum(pred_test == survived_test) / m;

end

function [g] = sigmoid (z)
    g = 1/ (1 + exp(-1 * z));
end